# Visitor Data Structure

Works by creating amalgamating various tables/views into a contiguous 
table or view.

The [visitor pattern](https://en.wikipedia.org/wiki/Visitor_pattern)
is present to ensure we can traverse over each created entity.

Yes I know that's rather complex, however this is useful in cases
where the company's data structure is complex, and you can't use
primary and foreign keys.

Example data warehouse where this is useful include Apache Hive, 
Google Big Query, Amazon Redshift and Apache Impala.
