package com.personal.VisitorDataStructure;

import java.util.HashSet;

public class Structure extends Child {
  final HashSet<Child> children;

  public Structure(final HashSet<Child> children, final String name, final Boolean isVirtual) {
    this.children = children;
    children.forEach(child -> this.columns.addAll(child.columns));
    this.name = name;
    this.isVirtual = isVirtual;
  }

  @Override
  public void accept(ViewOrTableVisitor viewOrTableVisitor) {
    children.forEach(child -> child.accept(viewOrTableVisitor));

    viewOrTableVisitor.visit(this);
  }

  @Override
  public String toString() {
    return children
        .stream()
        .map(child -> child.toString())
        .reduce((child1, child2) -> child1.concat(child2))
        .orElse("No Children");
  }
}
