package com.personal.VisitorDataStructure;

import java.util.HashSet;

public class Table extends Child {
  public Table(final String name, final HashSet<NameAndType> columns) {
    this.name = name;
    this.columns = columns;
  }

  @Override
  public void accept(ViewOrTableVisitor viewOrTableVisitor) {
    viewOrTableVisitor.visit(this);
  }
}
