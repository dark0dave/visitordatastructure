package com.personal.VisitorDataStructure;

import java.util.HashSet;

public class View extends Child {
  public View(final String name, final HashSet<NameAndType> columns) {
    this.name = name;
    this.columns = columns;
    this.isVirtual = true;
  }

  @Override
  public void accept(ViewOrTableVisitor viewOrTableVisitor) {
    viewOrTableVisitor.visit(this);
  }
}
