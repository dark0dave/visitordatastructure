package com.personal.VisitorDataStructure;

public interface ViewOrTableVisitor {
  void visit(View view);

  void visit(Table table);

  void visit(Structure structure);

  String getName();
}
