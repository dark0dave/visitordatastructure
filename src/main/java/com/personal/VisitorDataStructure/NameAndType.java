package com.personal.VisitorDataStructure;

class NameAndType {
  enum AllowedTypes {
    STRING,
    DATE,
    TIMESTAMP,
    LONG,
    INT,
    FLOAT,
    DECIMAL
  }

  final String dataType;
  final String name;

  NameAndType(final String name, final String dataType) {
    this.name = name;
    this.dataType = AllowedTypes.valueOf(dataType).toString();
  }
}
