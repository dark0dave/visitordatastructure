package com.personal.VisitorDataStructure;

import static java.lang.String.format;

import java.util.HashSet;

abstract class Child {
  public HashSet<NameAndType> columns = new HashSet<>();
  public String name;
  Boolean isVirtual = false;

  abstract void accept(ViewOrTableVisitor viewOrTableVisitor);

  @Override
  public String toString() {
    final String columnData =
        columns
            .stream()
            .map(columnName -> format("%s: Type of %s \n", columnName.name, columnName.dataType))
            .reduce((column1, column2) -> column1.concat(column2))
            .orElse("No Column Data");

    return format("---- %s: with Columns: ----\n%s", name, columnData);
  }
}
