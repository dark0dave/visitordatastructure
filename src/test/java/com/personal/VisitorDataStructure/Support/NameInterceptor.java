package com.personal.VisitorDataStructure.Support;

import com.personal.VisitorDataStructure.Structure;
import com.personal.VisitorDataStructure.Table;
import com.personal.VisitorDataStructure.View;
import com.personal.VisitorDataStructure.ViewOrTableVisitor;

public class NameInterceptor implements ViewOrTableVisitor {
  public final String visited = "Visited";

  @Override
  public void visit(View view) {
    view.name = visited + view.name;
  }

  @Override
  public void visit(Table table) {
    table.name = visited + table.name;
  }

  @Override
  public void visit(Structure structure) {
    structure.name = visited + structure.name;
  }

  @Override
  public String getName() {
    return "NameInterceptor";
  }
}
