package com.personal.VisitorDataStructure;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import com.personal.VisitorDataStructure.Support.NameInterceptor;
import java.util.HashSet;
import org.junit.Before;
import org.junit.Test;

public class StructureTest {

  final HashSet<NameAndType> nameAndTypes =
      new HashSet<>(asList(new NameAndType("Date", "DATE"), new NameAndType("UserId", "STRING")));

  final Table table = new Table("testTable1", nameAndTypes);
  final View view = new View("testTable2", nameAndTypes);
  final HashSet<Child> members = new HashSet<>(asList(table, view));

  Structure test;

  @Before
  public void setup() {
    test = new Structure(members, "simple", false);
  }

  @Test
  public void ensureChildrenExist() {
    assertThat(test.children.containsAll(members), is(true));
  }

  @Test
  public void onlyOneSetOfColumns() {
    assertThat(test.columns, equalTo(nameAndTypes));
    assertThat(test.toString().contains("testTable1"), is(true));
    assertThat(test.toString().contains("testTable2"), is(true));
  }

  @Test
  public void additionalViews() {
    final NameInterceptor nameInterceptor = new NameInterceptor();
    test.accept(nameInterceptor);

    test.children.forEach(
        child -> assertThat(child.toString().contains(nameInterceptor.visited), is(true)));
    assertThat(test.toString().contains(nameInterceptor.visited), is(true));
  }
}
