package com.personal.VisitorDataStructure;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.HashSet;
import org.junit.Before;
import org.junit.Test;

public class TableTest {

  Table table;

  @Before
  public void Setup() {
    table = new Table("testTable1", new HashSet<>());
  }

  @Test
  public void testToStringEmptyColumnData() {
    assertThat(table.toString(), equalTo("---- testTable1: with Columns: ----\nNo Column Data"));
  }

  @Test
  public void testToString() {
    final HashSet<NameAndType> types = new HashSet<>();
    types.add(new NameAndType("Date", "DATE"));

    final Table table = new Table("testTable1", types);

    assertThat(
        table.toString(), equalTo("---- testTable1: with Columns: ----\nDate: Type of DATE \n"));
  }

  @Test
  public void accept() {
    final Table annotherTable = table;
    // table.accept(annotherTable);
  }
}
